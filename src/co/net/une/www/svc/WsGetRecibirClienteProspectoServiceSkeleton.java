
/**
 * WsGetRecibirClienteProspectoServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsGetRecibirClienteProspectoServiceSkeleton java skeleton for the axisService
     */
    public class WsGetRecibirClienteProspectoServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsGetRecibirClienteProspectoServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param tipo_transaccion
                                     * @param codigo_prospecto
                                     * @param nombre_prospecto
                                     * @param codigo_direccion
                                     * @param telefono
                                     * @param deseo_contacto_cliente
                                     * @param marca_prospecto
                                     * @param estado_prospecto
                                     * @param listaProducto
                                     * @param fecha_creacion
                                     * @param id_proyecto
         */
        

                 public co.net.une.www.gis.WsGetRecibirClienteProspectoRSType mantenimientoClienteProspecto
                  (
                  java.lang.String tipo_transaccion,java.lang.String codigo_prospecto,java.lang.String nombre_prospecto,java.lang.String codigo_direccion,java.lang.String telefono,java.lang.String deseo_contacto_cliente,java.lang.String marca_prospecto,java.lang.String estado_prospecto,co.net.une.www.gis.ListaProductoType listaProducto,java.lang.String fecha_creacion,java.lang.String id_proyecto
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("tipo_transaccion",tipo_transaccion);params.put("codigo_prospecto",codigo_prospecto);params.put("nombre_prospecto",nombre_prospecto);params.put("codigo_direccion",codigo_direccion);params.put("telefono",telefono);params.put("deseo_contacto_cliente",deseo_contacto_cliente);params.put("marca_prospecto",marca_prospecto);params.put("estado_prospecto",estado_prospecto);params.put("listaProducto",listaProducto);params.put("fecha_creacion",fecha_creacion);params.put("id_proyecto",id_proyecto);
		try{
		
			return (co.net.une.www.gis.WsGetRecibirClienteProspectoRSType)
			this.makeStructuredRequest(serviceName, "mantenimientoClienteProspecto", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    