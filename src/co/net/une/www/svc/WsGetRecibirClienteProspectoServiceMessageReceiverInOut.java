
/**
 * WsGetRecibirClienteProspectoServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WsGetRecibirClienteProspectoServiceMessageReceiverInOut message receiver
        */

        public class WsGetRecibirClienteProspectoServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WsGetRecibirClienteProspectoServiceSkeleton skel = (WsGetRecibirClienteProspectoServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("mantenimientoClienteProspecto".equals(methodName)){
                
                co.net.une.www.gis.WsGetRecibirClienteProspectoRS wsGetRecibirClienteProspectoRS12 = null;
	                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedParam =
                                                             (co.net.une.www.gis.WsGetRecibirClienteProspectoRQ)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.gis.WsGetRecibirClienteProspectoRQ.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               wsGetRecibirClienteProspectoRS12 =
                                                   
                                                   
                                                           wrapmantenimientoClienteProspecto(
                                                       
                                                        

                                                        
                                                       skel.mantenimientoClienteProspecto(
                                                            
                                                                getTipo_transaccion(wrappedParam)
                                                            ,
                                                                getCodigo_prospecto(wrappedParam)
                                                            ,
                                                                getNombre_prospecto(wrappedParam)
                                                            ,
                                                                getCodigo_direccion(wrappedParam)
                                                            ,
                                                                getTelefono(wrappedParam)
                                                            ,
                                                                getDeseo_contacto_cliente(wrappedParam)
                                                            ,
                                                                getMarca_prospecto(wrappedParam)
                                                            ,
                                                                getEstado_prospecto(wrappedParam)
                                                            ,
                                                                getListaProducto(wrappedParam)
                                                            ,
                                                                getFecha_creacion(wrappedParam)
                                                            ,
                                                                getId_proyecto(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), wsGetRecibirClienteProspectoRS12, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.gis.WsGetRecibirClienteProspectoRQ param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.gis.WsGetRecibirClienteProspectoRQ.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.gis.WsGetRecibirClienteProspectoRS param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.gis.WsGetRecibirClienteProspectoRS.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.gis.WsGetRecibirClienteProspectoRS param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.gis.WsGetRecibirClienteProspectoRS.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private java.lang.String getTipo_transaccion(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getTipo_transaccion();
                            
                        }
                     

                        private java.lang.String getCodigo_prospecto(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getCodigo_prospecto();
                            
                        }
                     

                        private java.lang.String getNombre_prospecto(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getNombre_prospecto();
                            
                        }
                     

                        private java.lang.String getCodigo_direccion(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getCodigo_direccion();
                            
                        }
                     

                        private java.lang.String getTelefono(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getTelefono();
                            
                        }
                     

                        private java.lang.String getDeseo_contacto_cliente(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getDeseo_contacto_cliente();
                            
                        }
                     

                        private java.lang.String getMarca_prospecto(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getMarca_prospecto();
                            
                        }
                     

                        private java.lang.String getEstado_prospecto(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getEstado_prospecto();
                            
                        }
                     

                        private co.net.une.www.gis.ListaProductoType getListaProducto(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getListaProducto();
                            
                        }
                     

                        private java.lang.String getFecha_creacion(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getFecha_creacion();
                            
                        }
                     

                        private java.lang.String getId_proyecto(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                        
                                return wrappedType.getWsGetRecibirClienteProspectoRQ().getId_proyecto();
                            
                        }
                     
                        private co.net.une.www.gis.WsGetRecibirClienteProspectoRQType getmantenimientoClienteProspecto(
                        co.net.une.www.gis.WsGetRecibirClienteProspectoRQ wrappedType){
                            return wrappedType.getWsGetRecibirClienteProspectoRQ();
                        }
                        
                        
                    
                         private co.net.une.www.gis.WsGetRecibirClienteProspectoRS wrapmantenimientoClienteProspecto(
                            co.net.une.www.gis.WsGetRecibirClienteProspectoRSType innerType){
                                co.net.une.www.gis.WsGetRecibirClienteProspectoRS wrappedElement = new co.net.une.www.gis.WsGetRecibirClienteProspectoRS();
                                wrappedElement.setWsGetRecibirClienteProspectoRS(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.gis.WsGetRecibirClienteProspectoRQ.class.equals(type)){
                
                           return co.net.une.www.gis.WsGetRecibirClienteProspectoRQ.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.gis.WsGetRecibirClienteProspectoRS.class.equals(type)){
                
                           return co.net.une.www.gis.WsGetRecibirClienteProspectoRS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    